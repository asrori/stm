// Drawer
export const DRAWER = 'DRAWER'
export const MOBILE = 'MOBILE'

// Alert
export const SHOW_SNACKBAR = 'SHOW_SNACKBAR'
export const HIDE_SNACKBAR = 'HIDE_SNACKBAR'

// Auth
export const SET_TOKEN = 'SET_TOKEN'
export const SET_USER_DATA = 'SET_USER_DATA'
export const LOGOUT = 'LOGOUT'

// Status On/Off
export const ONLINE_STATUS = 'ONLINE_STATUS'
