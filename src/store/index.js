import Vue from 'vue'
import Vuex from 'vuex'
import {
  DRAWER,
  SHOW_SNACKBAR,
  HIDE_SNACKBAR,
  MOBILE,
  ONLINE_STATUS
} from './mutation-types'

Vue.use(Vuex)

const initialState = {
  is_dev: process.env.NODE_ENV === 'development',
  is_mobile: false,
  drawer: true,
  locale: 'id',
  snackbar: {
    show: false,
    timeout: 6000,
    message: null,
    color: 'black'
  },
  is_online: true
}

const state = initialState

const actions = {
  showSnackbar ({ commit }, snackbar) {
    commit(SHOW_SNACKBAR, snackbar)
    setTimeout(function () {
      commit(HIDE_SNACKBAR)
    }, 6000)
  },
  handleError ({ state, commit, dispatch, rootState, getters }, err) {
    if (err.response.status === 401) {
      dispatch('logout')
    }
  }
}

const mutations = {
  [DRAWER] (state, status) {
    state.drawer = status
  },
  [MOBILE] (state, status) {
    state.is_mobile = status
  },
  [SHOW_SNACKBAR] (state, snackbar) {
    state.snackbar = {
      show: true,
      timeout: 6000,
      color: snackbar.color ? snackbar.color : 'black',
      msg: snackbar.msg
    }
  },
  [HIDE_SNACKBAR] (state) {
    state.snackbar = initialState.snackbar
  },
  [ONLINE_STATUS] (state, status) {
    state.is_online = status
  }
}

const getters = {
  is_dev: state => {
    return state.is_dev
  },
  is_mobile: state => {
    return state.is_mobile
  },
  drawer: state => {
    return state.drawer
  },
  snackbar: state => {
    return state.snackbar
  },
  is_online: state => {
    return state.is_online
  }
}

export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters
})
